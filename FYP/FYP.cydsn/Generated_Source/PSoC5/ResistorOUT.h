/*******************************************************************************
* File Name: ResistorOUT.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ResistorOUT_H) /* Pins ResistorOUT_H */
#define CY_PINS_ResistorOUT_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "ResistorOUT_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 ResistorOUT__PORT == 15 && ((ResistorOUT__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    ResistorOUT_Write(uint8 value);
void    ResistorOUT_SetDriveMode(uint8 mode);
uint8   ResistorOUT_ReadDataReg(void);
uint8   ResistorOUT_Read(void);
void    ResistorOUT_SetInterruptMode(uint16 position, uint16 mode);
uint8   ResistorOUT_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the ResistorOUT_SetDriveMode() function.
     *  @{
     */
        #define ResistorOUT_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define ResistorOUT_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define ResistorOUT_DM_RES_UP          PIN_DM_RES_UP
        #define ResistorOUT_DM_RES_DWN         PIN_DM_RES_DWN
        #define ResistorOUT_DM_OD_LO           PIN_DM_OD_LO
        #define ResistorOUT_DM_OD_HI           PIN_DM_OD_HI
        #define ResistorOUT_DM_STRONG          PIN_DM_STRONG
        #define ResistorOUT_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define ResistorOUT_MASK               ResistorOUT__MASK
#define ResistorOUT_SHIFT              ResistorOUT__SHIFT
#define ResistorOUT_WIDTH              1u

/* Interrupt constants */
#if defined(ResistorOUT__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ResistorOUT_SetInterruptMode() function.
     *  @{
     */
        #define ResistorOUT_INTR_NONE      (uint16)(0x0000u)
        #define ResistorOUT_INTR_RISING    (uint16)(0x0001u)
        #define ResistorOUT_INTR_FALLING   (uint16)(0x0002u)
        #define ResistorOUT_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define ResistorOUT_INTR_MASK      (0x01u) 
#endif /* (ResistorOUT__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define ResistorOUT_PS                     (* (reg8 *) ResistorOUT__PS)
/* Data Register */
#define ResistorOUT_DR                     (* (reg8 *) ResistorOUT__DR)
/* Port Number */
#define ResistorOUT_PRT_NUM                (* (reg8 *) ResistorOUT__PRT) 
/* Connect to Analog Globals */                                                  
#define ResistorOUT_AG                     (* (reg8 *) ResistorOUT__AG)                       
/* Analog MUX bux enable */
#define ResistorOUT_AMUX                   (* (reg8 *) ResistorOUT__AMUX) 
/* Bidirectional Enable */                                                        
#define ResistorOUT_BIE                    (* (reg8 *) ResistorOUT__BIE)
/* Bit-mask for Aliased Register Access */
#define ResistorOUT_BIT_MASK               (* (reg8 *) ResistorOUT__BIT_MASK)
/* Bypass Enable */
#define ResistorOUT_BYP                    (* (reg8 *) ResistorOUT__BYP)
/* Port wide control signals */                                                   
#define ResistorOUT_CTL                    (* (reg8 *) ResistorOUT__CTL)
/* Drive Modes */
#define ResistorOUT_DM0                    (* (reg8 *) ResistorOUT__DM0) 
#define ResistorOUT_DM1                    (* (reg8 *) ResistorOUT__DM1)
#define ResistorOUT_DM2                    (* (reg8 *) ResistorOUT__DM2) 
/* Input Buffer Disable Override */
#define ResistorOUT_INP_DIS                (* (reg8 *) ResistorOUT__INP_DIS)
/* LCD Common or Segment Drive */
#define ResistorOUT_LCD_COM_SEG            (* (reg8 *) ResistorOUT__LCD_COM_SEG)
/* Enable Segment LCD */
#define ResistorOUT_LCD_EN                 (* (reg8 *) ResistorOUT__LCD_EN)
/* Slew Rate Control */
#define ResistorOUT_SLW                    (* (reg8 *) ResistorOUT__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define ResistorOUT_PRTDSI__CAPS_SEL       (* (reg8 *) ResistorOUT__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define ResistorOUT_PRTDSI__DBL_SYNC_IN    (* (reg8 *) ResistorOUT__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define ResistorOUT_PRTDSI__OE_SEL0        (* (reg8 *) ResistorOUT__PRTDSI__OE_SEL0) 
#define ResistorOUT_PRTDSI__OE_SEL1        (* (reg8 *) ResistorOUT__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define ResistorOUT_PRTDSI__OUT_SEL0       (* (reg8 *) ResistorOUT__PRTDSI__OUT_SEL0) 
#define ResistorOUT_PRTDSI__OUT_SEL1       (* (reg8 *) ResistorOUT__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define ResistorOUT_PRTDSI__SYNC_OUT       (* (reg8 *) ResistorOUT__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(ResistorOUT__SIO_CFG)
    #define ResistorOUT_SIO_HYST_EN        (* (reg8 *) ResistorOUT__SIO_HYST_EN)
    #define ResistorOUT_SIO_REG_HIFREQ     (* (reg8 *) ResistorOUT__SIO_REG_HIFREQ)
    #define ResistorOUT_SIO_CFG            (* (reg8 *) ResistorOUT__SIO_CFG)
    #define ResistorOUT_SIO_DIFF           (* (reg8 *) ResistorOUT__SIO_DIFF)
#endif /* (ResistorOUT__SIO_CFG) */

/* Interrupt Registers */
#if defined(ResistorOUT__INTSTAT)
    #define ResistorOUT_INTSTAT            (* (reg8 *) ResistorOUT__INTSTAT)
    #define ResistorOUT_SNAP               (* (reg8 *) ResistorOUT__SNAP)
    
	#define ResistorOUT_0_INTTYPE_REG 		(* (reg8 *) ResistorOUT__0__INTTYPE)
#endif /* (ResistorOUT__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_ResistorOUT_H */


/* [] END OF FILE */
