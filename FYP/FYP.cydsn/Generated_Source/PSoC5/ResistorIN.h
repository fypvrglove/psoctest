/*******************************************************************************
* File Name: ResistorIN.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ResistorIN_H) /* Pins ResistorIN_H */
#define CY_PINS_ResistorIN_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "ResistorIN_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 ResistorIN__PORT == 15 && ((ResistorIN__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    ResistorIN_Write(uint8 value);
void    ResistorIN_SetDriveMode(uint8 mode);
uint8   ResistorIN_ReadDataReg(void);
uint8   ResistorIN_Read(void);
void    ResistorIN_SetInterruptMode(uint16 position, uint16 mode);
uint8   ResistorIN_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the ResistorIN_SetDriveMode() function.
     *  @{
     */
        #define ResistorIN_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define ResistorIN_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define ResistorIN_DM_RES_UP          PIN_DM_RES_UP
        #define ResistorIN_DM_RES_DWN         PIN_DM_RES_DWN
        #define ResistorIN_DM_OD_LO           PIN_DM_OD_LO
        #define ResistorIN_DM_OD_HI           PIN_DM_OD_HI
        #define ResistorIN_DM_STRONG          PIN_DM_STRONG
        #define ResistorIN_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define ResistorIN_MASK               ResistorIN__MASK
#define ResistorIN_SHIFT              ResistorIN__SHIFT
#define ResistorIN_WIDTH              1u

/* Interrupt constants */
#if defined(ResistorIN__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ResistorIN_SetInterruptMode() function.
     *  @{
     */
        #define ResistorIN_INTR_NONE      (uint16)(0x0000u)
        #define ResistorIN_INTR_RISING    (uint16)(0x0001u)
        #define ResistorIN_INTR_FALLING   (uint16)(0x0002u)
        #define ResistorIN_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define ResistorIN_INTR_MASK      (0x01u) 
#endif /* (ResistorIN__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define ResistorIN_PS                     (* (reg8 *) ResistorIN__PS)
/* Data Register */
#define ResistorIN_DR                     (* (reg8 *) ResistorIN__DR)
/* Port Number */
#define ResistorIN_PRT_NUM                (* (reg8 *) ResistorIN__PRT) 
/* Connect to Analog Globals */                                                  
#define ResistorIN_AG                     (* (reg8 *) ResistorIN__AG)                       
/* Analog MUX bux enable */
#define ResistorIN_AMUX                   (* (reg8 *) ResistorIN__AMUX) 
/* Bidirectional Enable */                                                        
#define ResistorIN_BIE                    (* (reg8 *) ResistorIN__BIE)
/* Bit-mask for Aliased Register Access */
#define ResistorIN_BIT_MASK               (* (reg8 *) ResistorIN__BIT_MASK)
/* Bypass Enable */
#define ResistorIN_BYP                    (* (reg8 *) ResistorIN__BYP)
/* Port wide control signals */                                                   
#define ResistorIN_CTL                    (* (reg8 *) ResistorIN__CTL)
/* Drive Modes */
#define ResistorIN_DM0                    (* (reg8 *) ResistorIN__DM0) 
#define ResistorIN_DM1                    (* (reg8 *) ResistorIN__DM1)
#define ResistorIN_DM2                    (* (reg8 *) ResistorIN__DM2) 
/* Input Buffer Disable Override */
#define ResistorIN_INP_DIS                (* (reg8 *) ResistorIN__INP_DIS)
/* LCD Common or Segment Drive */
#define ResistorIN_LCD_COM_SEG            (* (reg8 *) ResistorIN__LCD_COM_SEG)
/* Enable Segment LCD */
#define ResistorIN_LCD_EN                 (* (reg8 *) ResistorIN__LCD_EN)
/* Slew Rate Control */
#define ResistorIN_SLW                    (* (reg8 *) ResistorIN__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define ResistorIN_PRTDSI__CAPS_SEL       (* (reg8 *) ResistorIN__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define ResistorIN_PRTDSI__DBL_SYNC_IN    (* (reg8 *) ResistorIN__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define ResistorIN_PRTDSI__OE_SEL0        (* (reg8 *) ResistorIN__PRTDSI__OE_SEL0) 
#define ResistorIN_PRTDSI__OE_SEL1        (* (reg8 *) ResistorIN__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define ResistorIN_PRTDSI__OUT_SEL0       (* (reg8 *) ResistorIN__PRTDSI__OUT_SEL0) 
#define ResistorIN_PRTDSI__OUT_SEL1       (* (reg8 *) ResistorIN__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define ResistorIN_PRTDSI__SYNC_OUT       (* (reg8 *) ResistorIN__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(ResistorIN__SIO_CFG)
    #define ResistorIN_SIO_HYST_EN        (* (reg8 *) ResistorIN__SIO_HYST_EN)
    #define ResistorIN_SIO_REG_HIFREQ     (* (reg8 *) ResistorIN__SIO_REG_HIFREQ)
    #define ResistorIN_SIO_CFG            (* (reg8 *) ResistorIN__SIO_CFG)
    #define ResistorIN_SIO_DIFF           (* (reg8 *) ResistorIN__SIO_DIFF)
#endif /* (ResistorIN__SIO_CFG) */

/* Interrupt Registers */
#if defined(ResistorIN__INTSTAT)
    #define ResistorIN_INTSTAT            (* (reg8 *) ResistorIN__INTSTAT)
    #define ResistorIN_SNAP               (* (reg8 *) ResistorIN__SNAP)
    
	#define ResistorIN_0_INTTYPE_REG 		(* (reg8 *) ResistorIN__0__INTTYPE)
#endif /* (ResistorIN__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_ResistorIN_H */


/* [] END OF FILE */
