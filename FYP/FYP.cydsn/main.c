/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

int adc_result = 0;
int sel = 0;
int print_flag = 0;
char debug[200];
int adc_val[10] = {};

void AdcConvert();

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    UART_Start();
    Opamp_Start();
    ADC_Start();
    Mux_Start();
    
    ADC_StartConvert();
    Mux_Select(0);
    
    for(;;)
    {
        AdcConvert();
        if (1) {
            sprintf(debug,\
                "A0 = %d\tA1 = %d\tA2 = %d\tA3 = %d\tA4 = %d\tA5 = %d\tA6 = %d\tA7 = %d\tA8 = %d\tA9 = %d\n",\
                adc_val[0],adc_val[1],adc_val[2],adc_val[3],adc_val[4],adc_val[5],adc_val[6],adc_val[7],adc_val[8],adc_val[9]);
            UART_PutString(debug);
        }
    }
}

void AdcConvert() {
    if (ADC_IsEndConversion(ADC_RETURN_STATUS)) {
        adc_result = ADC_GetResult16();
        switch (sel) {
            case 0: adc_val[0] = adc_result;
                    break;
            case 1: adc_val[1] = adc_result;
                    break;
            case 2: adc_val[2] = adc_result;
                    break;
            case 3: adc_val[3] = adc_result;
                    break;
            case 4: adc_val[4] = adc_result;
                    break;
            case 5: adc_val[5] = adc_result;
                    break;
            case 6: adc_val[6] = adc_result;
                    break;
            case 7: adc_val[7] = adc_result;
                    break;
            case 8: adc_val[8] = adc_result;
                    break;
            case 9: adc_val[9] = adc_result;
                    break;
            default: adc_val[0] = adc_result;
      
        }
        sel++;
        if (sel > 9) sel = 0;
        
        Mux_Select(sel);
    }
}

/* [] END OF FILE */
